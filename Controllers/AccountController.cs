﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LoginApp.Models;
using Microsoft.EntityFrameworkCore;
using System.Web;
using Microsoft.AspNetCore.Session;
using MimeKit;
using MailKit.Net.Smtp;

namespace LoginApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly ApplicationUserClass _applicationUserClass;
        public bool authenticated;

        public AccountController(ApplicationUserClass applicationUserClass)
        {
            _applicationUserClass = applicationUserClass;
        }

        public IActionResult Index()
        {
            ViewBag.message = "You have successfully logged in";
            
            return View(_applicationUserClass.UserDetails.ToList());
        }
        public IActionResult Login()
        {
            return View();
        }
       
        public IActionResult Edit()
        {
            var EmailAddress = TempData["value"].ToString();
            TempData["value"] = EmailAddress;
            return View(_applicationUserClass.UserDetails.Where(x=>x.EmailAddress==EmailAddress).FirstOrDefault());
        }

        [HttpPost]
        public IActionResult Edit(ApplicationUser applicationUser)
        {
            var EmailAddress = TempData["value"].ToString();
            var user = _applicationUserClass.UserDetails.Where(x => x.EmailAddress == EmailAddress).FirstOrDefault();
            user.FirstName = applicationUser.FirstName;
            user.LastName = applicationUser.LastName;
            user.DateOfBirth = applicationUser.DateOfBirth;
            user.Gender = applicationUser.Gender;
            user.EmailAddress = applicationUser.EmailAddress;
            _applicationUserClass.UserDetails.Update(user);
            _applicationUserClass.SaveChanges();
            return RedirectToAction("Index");
        }

        public IActionResult ForgotPassword()
        {
            return View();
        }
        public IActionResult LogOut()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Login");
        }
        public IActionResult Delete()
        {
            var EmailAddress = TempData["value"].ToString();
            TempData["value"] = EmailAddress;
            return View(_applicationUserClass.UserDetails.Where(x => x.EmailAddress == EmailAddress).FirstOrDefault());
        }

        [HttpPost]
        public IActionResult Delete(ApplicationUser applicationUser)
        {
            var EmailAddress = TempData["value"].ToString();
            var user=_applicationUserClass.UserDetails.Where(x => x.EmailAddress == EmailAddress).FirstOrDefault();
            _applicationUserClass.UserDetails.Remove(user);
            _applicationUserClass.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult ForgotPassword(ApplicationUser applicationUser)
        {
            if (!String.IsNullOrEmpty(Request.Form["Passwordvalue"].ToString()) && !String.IsNullOrEmpty(Request.Form["ConfirmPassword"].ToString()) && !String.IsNullOrEmpty( Request.Form["EmailAddress"].ToString()) && Request.Form["Passwordvalue"].ToString() == Request.Form["ConfirmPassword"].ToString())
            {
                var EmailAddress = Request.Form["EmailAddress"].ToString();
                var PasswordResponse = Request.Form["Passwordvalue"].ToString();
                var user = _applicationUserClass.UserDetails.Where(x => x.EmailAddress == EmailAddress).FirstOrDefault();
                if (user != null)
                {
                    applicationUser.FirstName = user.FirstName;
                    applicationUser.LastName = user.LastName;
                    applicationUser.DateOfBirth = user.DateOfBirth;
                    applicationUser.Gender = user.Gender;
                    applicationUser.EmailAddress = user.EmailAddress;
                    applicationUser.Password = PasswordResponse;
                    user.Password = PasswordResponse;
                    _applicationUserClass.UserDetails.Update(user);
                    _applicationUserClass.SaveChanges();
                    ViewBag.Message = "Password Successfully changed. Kindly Login";
                }
                else
                {
                    ViewBag.Message = "Email Address is not registered. Please SignUP.";
                }
            }
            else
            {
                ViewBag.Message = "Kindly Provide Correct Details.";
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(LoginClass loginClass)
        {
            var response = Request.Form.ToList();
            var count = 0;
            foreach (var keyValuePair in response)
            {
                if (keyValuePair.Key == "g-recaptcha-response" & !String.IsNullOrEmpty(keyValuePair.Value))
                {
                    count += 1;
                    continue;
                }

            }
            if (count == 0)
            {
                ViewBag.Message = "Verify that you are human to proceed";
                return View();
            }
            var value = loginClass.EmailAddress.ToString();
            TempData["value"] = value;
            var results = _applicationUserClass.UserDetails.Where(a => a.EmailAddress == value).FirstOrDefault();
            if (results == null){
                ViewBag.Message = "Email Address not registered. Please SingUp.";
            }
            if (results.EmailAddress==loginClass.EmailAddress && (DecryptString(results.Password)==loginClass.Password))
            {
               return  RedirectToAction("Index");

            }
            else
            {
                ViewBag.message = "Invalid Email Address or password";
                return View();
            }
            
        }
        public string DecryptString(string encrString)
        {
            byte[] b;
            string decrypted;
            try
            {
                b = Convert.FromBase64String(encrString);
                decrypted = System.Text.ASCIIEncoding.ASCII.GetString(b);
            }
            catch (FormatException fe)
            {
                decrypted = "";
            }
            return decrypted;
        }
    }
}