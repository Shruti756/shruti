﻿using LoginApp.Models;
using Microsoft.AspNetCore.Server;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using MailKit.Net.Smtp;
using MimeKit;
using System.IO;
using System.Web;
using Microsoft.AspNetCore.Hosting.Server;

namespace LoginApp.Controllers
{
    public class UserRegistrationController : Controller
    {
        private readonly ApplicationUserClass _applicationUserClass;

        public UserRegistrationController(ApplicationUserClass applicationUserClass)
        {
            _applicationUserClass = applicationUserClass;
        }

        public IActionResult ActivationCode(ApplicationUser applicationUser)
        {
            var activationlink = "https://localhost:44331/UserRegistration/PasswordEntry";
            var EmailAddress = applicationUser.EmailAddress;
            TempData["EmailAddress"] = applicationUser.EmailAddress;
            var message = new MimeMessage();
            var fromaddress = "shruti.sharma@geminisolutions.in";
            message.From.Add(new MailboxAddress(fromaddress));
            message.To.Add(new MailboxAddress(EmailAddress));
            message.Subject = applicationUser.FirstName + " Please find activation Link attached";
            message.Body = new TextPart("html")
                {
                    Text = "Activation link =    " + activationlink + "  .Please enter this code to activate the email address"
                };
            SmtpClient client = new SmtpClient();
            client.Connect("smtp.gmail.com", 587);
            client.Authenticate(fromaddress, "Shruti1$");
            client.Send(message);
            client.Disconnect(false);
            _applicationUserClass.Add(applicationUser);
            var success = _applicationUserClass.SaveChanges();
            return RedirectToAction("Info");
        }

        public IActionResult Info()
        {

            return View();
        }

        public IActionResult PasswordEntry()
        {
           
            return View();
        }

        [HttpPost]
        public IActionResult PasswordEntry(ApplicationUser applicationUser )
        {
            if (!String.IsNullOrEmpty(Request.Form["Passwordvalue"].ToString()) && !String.IsNullOrEmpty(Request.Form["ConfirmPassword"].ToString()) && Request.Form["Passwordvalue"].ToString() == Request.Form["ConfirmPassword"].ToString())
            {
                    var PasswordResponse = Request.Form["Passwordvalue"].ToString();
                        var EmailAddress = TempData["EmailAddress"].ToString();
                        var user = _applicationUserClass.UserDetails.Where(x => x.EmailAddress == EmailAddress).FirstOrDefault();                       
                            applicationUser.Status = "Verified";
                            user.Status = applicationUser.Status;
                            applicationUser.FirstName = user.FirstName;
                            applicationUser.LastName = user.LastName;
                            applicationUser.DateOfBirth = user.DateOfBirth;
                            applicationUser.Gender = user.Gender;
                            user.Password = EnryptString(PasswordResponse);
                            applicationUser.Password = user.Password;
                            _applicationUserClass.UserDetails.Update(user);
                            _applicationUserClass.SaveChanges();
                            ViewBag.Message = "You have successfully signed Up. Please go to login Page to access details";
            }
            else
            {
                ViewBag.Message = "Please provide correct details";
            }
                return View();
         }
        

        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(ApplicationUser applicationUser )
        {
            var response = Request.Form.ToList();
            var count = 0;
            foreach (var value in response)
            {
                if(value.Key=="g-recaptcha-response" & !String.IsNullOrEmpty(value.Value))
                {
                    count += 1;
                    continue;
                }

            }
            if(count==0)
            {
                ViewBag.Message = "Verify that you are human to proceed";
                return View();
            }
            var user = _applicationUserClass.UserDetails.Where(x => x.EmailAddress == applicationUser.EmailAddress).FirstOrDefault();
            if (user != null)
            {
                ViewBag.Message = "Email Address already Exists. Please Login.";
                return View();
            }
            else
            {
                var filename = Request.Form["UploadImage"];
                applicationUser.UploadImage = "/User-Images/" + filename;
                applicationUser.Status = "Not Verified";
                return RedirectToAction("ActivationCode", applicationUser);
            }

        }
        public string EnryptString(string strEncrypted)
        {
            byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(strEncrypted);
            string encrypted = Convert.ToBase64String(b);
            return encrypted;
        }
    }

}