﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace LoginApp.Models
{
    public class ApplicationUser
    {
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "You are required to fill all the details before submitting the form")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "You are required to fill all the details before submitting the form")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "You are required to fill all the details before submitting the form")]
        [DataType(DataType.Date)]
        public string DateOfBirth { get; set; }
        [Required(ErrorMessage = "You are required to fill all the details before submitting the form")]
        public string Gender { get; set; }
        [Required(ErrorMessage = "You are required to fill all the details before submitting the form")]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }
        [Required(ErrorMessage = "You are required to fill all the details before submitting the form")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required(ErrorMessage = "You are required to fill all the details before submitting the form")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
        public string Status { get; set; }
        [Required(ErrorMessage = "You are required to fill all the details before submitting the form")]
        public string UploadImage { get; set; }
    }
}
