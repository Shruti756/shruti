﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LoginApp.Models
{
    public class LoginClass
    {
        [Required(ErrorMessage ="Please fill all the required details")]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }
        [Required(ErrorMessage = "Please fill all the required details")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
